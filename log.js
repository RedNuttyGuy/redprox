'use strict'

const fs = require('fs')

const log = function(data) {
  fs.appendFile(exports.logFile, `${data}\n`, (err) => {
    if(err) {
      throw err
    }
    console.log(data)
  })
}

var exports = module.exports = {
  logFile: '/proxy/app/logs/proxy.log',
  
  logLevel: 2,
  // 0: none
  // 1: error
  // 2: reject
  // 3: info
  
  
  
  info: async function(data) {
    if(exports.logLevel >= 3) {
      log(data)
    }
  },
  
  reject: async function(data) {
    if(exports.logLevel >= 2) {
      log(data)
    }
  },
  
  error: async function(data) {
    if(exports.logLevel >= 1) {
      log(data)
    }
  },
  
  rotate: function() {
    fs.writeFileSync(this.logFile, '')
    console.log('Rotating Log File')
  }
}