const http = require('http')
const https = require('https')

const fs = require('fs')
fs.readdirRecursive = require("recursive-readdir")

const url = require('url')

const auth = require('./auth.js')
const route = require('./route.js')
const assembler = require('./assembler.js')

var guiServer

const server = module.exports = {
  startGui: async function() {

    let cert = await auth.generatePem()
    let srv = {}
    let srvDir = `${__dirname}/srv`

    let dir = await new Promise((resolve, reject) => {
      fs.readdirRecursive(srvDir, (err, files) => {
        if (err) {
          reject(err)
        } else {
          resolve(files)
        }
      })
    })
    
    // for (let f in dir) {
      // let fname = dir[f]
      // let fpath = fname.replace(srvDir, '')
      // srv[fpath] = new Promise((resolve, reject) => {
        // fs.readFile(fname, (err, data) => {
          // if(err) {
            // reject(err)
          // } else {
            // resolve(data)
          // }
        // })
      // }).then((data) => {
        // srv[fpath] = data
      // }).catch((err) => {
        // srv[fpath] = ''
      // })
    // }
    
    for(let f in dir) {
      
      let fname = dir[f]
      let fpath = fname.replace(srvDir, '')
      
      srv[fpath] = fname
      
    }
    
    Promise.all(Object.values(srv)).then(() => {
      guiServer = http.createServer(/*{key: cert.serviceKey, cert: cert.certificate}, */async (req, res) => {
        let reqUrl = url.parse(`http://${req.headers.host}${req.url}`).pathname
        if(req.method === 'POST') {
          let data = ''
          req.on('data', (chunk) => {
            data += chunk.toString()
          })
          
          req.on('end', async () => {
            
            data = JSON.parse(data)
            
            let args = data.args
            
            let result = await route.api(reqUrl, (args.args) ? args.args : [])
            res.writeHead(200, {'Content-Type':'application/json'})
            res.end(JSON.stringify(result))
          })
          
        } else if (req.method === 'GET') {
          if(reqUrl == '/') {
            if(!req.finished && !res.finished) {
              res.writeHead(200, {'Content-Type':'text/html'})
              let page = await assembler.generatePage()
              res.end(page)
            }
          } else if (srv[reqUrl]) {
            if(!req.finished && !res.finished) {
              let mime = reqUrl.split('.')
              mime = mimeTypes['.' + mime[mime.length-1]]
              
              res.writeHead(200, {'Content-Type': mime})
              res.end(fs.readFileSync(srv[req.url]))
            }
          } else {
            if(!req.finished && !res.finished) {
              res.writeHead(404, {'Content-Type':'text/html'})
              res.end(srv['/404.html'])
            }
          }
        }
      }).listen(options.gui.port, options.gui.interface)
    })
  },
  stopGui: function() {
    if(guiServer) {
      console.log(guiServer)
      guiServer.close()
    }
  }
}