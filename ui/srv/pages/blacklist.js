let blacklist = {
  onOpen: function() {
    let bl = req.getData('/api/blacklist/blacklist')

    Promise.resolve(bl).then((d) => {
      
      let thead = ['IP Address', '']
      
      let tbody = []
      
      for (let i in d) {
        
        let ip = d[i]
        
        let whois = `<a class="btn tooltip" href="https://www.whois.com/whois/${ip}" target="_blank"><i class="fas fa-search"></i><span class="tooltiptext">Lookup</span></a>`
        let btn = `<btn class="tooltip" onClick="req.getData('/api/blacklist/remove', {args:[${ip}]})"><i class="fas fa-times"></i><span class="tooltiptext">Remove</span></btn>`
        
        tbody.push([ip, whois + btn])
      }
      this.data.blacklist = d
      util.dataTable.display('#blacklistDisplay', thead, tbody)
      this.data.tableRows = document.querySelectorAll('#blacklistDisplay tbody tr')
      util.dataTable.pagenate('#blacklistDisplay', 30)
      this.addSearch(document.querySelector('#blacklistSearch input'))
    })
  },
  data: {},
  addSearch: function(i) {

   let input
    
    if(typeof i == 'string') {
      input = document.querySelector(i)
    } else {
      input = i
    }
      
    let results = input.parentElement.querySelector('.results')
    
    
    input.addEventListener('input', (e) => {
      
      let searchTerm = input.value
      
      if(!searchTerm) {
        results.style.display = 'none'
        return
      } else {
        results.style.display = ''
      }
      
      let resultsList = '<tbody>'
      let counter = 0
      for(let i in this.data.blacklist) {
        
        if(counter >= 5) {
          break
        }
        
        if(this.data.blacklist[i].includes(searchTerm)) {
          let html = this.data.tableRows[i].innerHTML.replace(searchTerm, `<strong>${searchTerm}</strong>`)
          resultsList += `<tr>${html}</tr>`
          counter++
        }
        
      }
      resultsList += '</tbody>'
      results.innerHTML = resultsList
    })
    
    document.addEventListener('click', (e) => {
      if(e.target == input.parentElement || input.parentElement.contains(e.target)) {
        if(input.value) {
          results.style.display = ''
        }
      } else {
        results.style.display = 'none'
      }
    })
  }
  
}

export {blacklist}