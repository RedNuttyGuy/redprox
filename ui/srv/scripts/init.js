var __mainContent
var __currentPage = ''

var pages = {}

const init = {
  onHashChange: function() {
    let page = location.hash.split('#')[1]
    pages4.setNavCss(page)
    pages4.displayPage(page)
  },
  cachePages: async function() {
    return new Promise((resolve, reject) => {
      let promises = []
      
      let pageList = pages4.getPagesList()
      
      for(let p in pageList) {
        let page = pageList[p]
        pages[page] = {}
      }
      
      for(let p in pages) {
        
        let pHTML = pages4.getPageHTML(p)
        pHTML.then((d) => {
          pages[p].html = d
        })
        
        let pJS = pages4.getPageJS(p)
        pJS.then((d) => {
          if(d) {
            for(let k of Object.keys(d)) {
              pages[p][k] = d[k]
            }
          }
        })
        
        promises.push(pHTML)
        promises.push(pJS)
        
      }
      Promise.all(promises).then(() => {
        resolve('')
      })
    })
  }
}

window.onhashchange = init.onHashChange
window.onload = async function() {
  __navbar = document.querySelector('#nav')
  __mainContent = document.querySelector('#main')
  
  await init.cachePages()
  

  
  if(!location.hash) {
    location.hash = '#dashboard'
  }

  init.onHashChange()
}