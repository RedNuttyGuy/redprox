const req = {
  getData: async function(url, data={args:[]}, method='POST') {
    
    let options = {
      "method": method
    }
    
    if(data) {
      options.body = JSON.stringify(data)
    }
    
    let request = await fetch(url, options)
    
    if(request.status!=200){
      console.error(request.status)
      return
    }
    
    return await request.json()
    
    
  }
}