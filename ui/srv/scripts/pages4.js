
const pages4 = {
  getPagesList: function() {
    let links = document.querySelectorAll('nav a')
    let pages = []
    for(let link of links) {
      pages.push(link.href.split('#')[1])
    }
    return pages
  },
  getPageHTML: async function(page) {
    return new Promise(async (resolve, reject)=> {
      let request = await fetch(`/pages/${page}.html`)
    
      if(request.status != 200) {
        console.error(request.status)
        reject('')
      }
    
      resolve(await request.text())
      
    })
    
  },
  getPageJS: async function(page) {
    
    return new Promise((resolve, reject) => {
      import(`/pages/${page}.js`).then((d) => {
        resolve(d[page])
      }).catch((err) => {
        reject('')
      })
    })
    
  },
  displayPage: async function(page) {
    
    let guid = page + new Date().getTime()
    __pendingPageGuid = guid
    
    if(page != __currentPage) {

      __mainContent.innerHTML = pages[page].html
      if(pages[page].onOpen) {
        pages[page].onOpen(page)
      }
    }
  },
  setNavCss: function(page) {
    let node = document.querySelector(`nav a[href="#${page}"]`)
    
    let nodes = Array.from(document.querySelectorAll('nav a.active'))
    
    for (let n in nodes) {
      nodes[n].classList.remove('active')
    }
    
    node.classList.add('active')
  }
}