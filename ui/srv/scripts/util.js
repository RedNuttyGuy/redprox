const util = {
  
  dataTable: {
    display: function(t, header='', body='', footer='') {
      
      let table = document.querySelector(t+'.dataTable')
      
      if(!table) {
        return false
      }
      
      let thead = ''
      let tbody = ''
      let tfoot = ''
      
      if(header) {
        thead = '<thead><tr>'
        for(let h in header){
          thead += `<td>${header[h]}</td>`
        }
        thead += '</tr></thead>'
      }
      
      if(body) {
        tbody = '<tbody>'
        for(let r in body) {
          let row = body[r]
          tbody += '<tr>'
          for(let d in row) {
            tbody += `<td>${row[d]}</td>`
          }
          tbody += '</tr>'
        }
        tbody += '</tbody>'
      }

      table.innerHTML = thead + tbody
      
      return true
    },
    pagenate: function(t, items=25) {
      
      let table
      
      if(typeof t == 'string') {
        table = document.querySelector(t)
      } else {
        table = t
      }
      let rows = table.querySelectorAll('tbody tr')
      let cols = rows[0].childNodes.length
      let pageCount = Math.ceil(rows.length / items)
      
      let tfoot = `<tfoot><tr><td colspan='${cols}'>`

      for(let r=0; r < rows.length; r++) {
        
        let p = Math.floor(r / items)
        rows[r].setAttribute('page', p)
        
      }
      
      for(let p=0; p < pageCount; p++) {
        
        tfoot += `<btn onclick='util.dataTable.changePage(${p}, "${t}", this)'>${p+1}</btn>`        
      }
      
      tfoot += '</td></tr></tfoot>'
      
      table.innerHTML += tfoot
      
      util.dataTable.changePage(0, table)
      
    },
    changePage: function(page, t) {
      
      let table
      
      if(typeof t == 'string') {
        table = document.querySelector(t)
      } else {
        table = t
      }

      let rows = table.querySelectorAll(`tbody tr`)
      
      for(let r=0; r < rows.length; r++) {

        if(rows[r].getAttribute('page') == page) {
          rows[r].classList.remove('notInPage')
        } else {
          rows[r].classList.add('notInPage')
        }
      }
      
    }
  },
  inRange: function(val, min, max) {
    return ((val - min) * (val - max) <= 0)
    
    
    
    
    
  },
  wait: async function(time) {
    
    return new Promise((resolve, reject) => {
      setTimeout(function() {
        resolve(true)
      }, time)
    })
    
  },
  changeStyleAsync: async function(element, style, value) {
    element.style[style] = value
    
  }
  
  
}
