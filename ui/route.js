var commands = {}
commands['auth'] = require('./auth.js')
commands['blacklist'] = require('../blacklist.js')
commands['config'] = require('../config.js')
commands['connection'] = require('../connection.js')
commands['init'] = require('../init.js')

function getProperty(propertyName, object) {
  let property = object,
      parts = propertyName.split('.')
  for(let i = 0; i < parts.length; i++) {
    property = property[parts[i]]
  }
  return property
}

const route = module.exports = {
  api: async function(path, args, token=null) {
    let p = path.split('/')
    let p2 = []
    for(let i in p) {
      if(p[i] != '' && p[i] != 'api' ) {
        p2.push(p[i])
      } 
    }
    
    let command = p2.join('.')
    
    let authorized = true
    
    if(command == 'auth.validate.user') {
      authorized = commands.auth.validate.user.apply(null, args)
      return authorized
      
    } else if(token){
      authorized = commands.auth.validate(token)
    }
    if(authorized) {
      
      if(options.commands.enabled.includes(command)) {
        
        let f = getProperty(command, commands)
        
        let result = f.apply(null, args)
        let response
        
        if(result instanceof Promise) {
          response = await result
        } else {
          response = result
        }
        return response
      }
    }
    return
    
    
  }
}