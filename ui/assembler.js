const fs = require('fs')

const assembler = module.exports = {
  
  generateNavbar: function() {
    
    let navbar
    let nodes = []
    
    for(let n in options.gui.pages) {
      
      let node = options.gui.pages[n]
      
      nodes.push(`<a href="${node.href}"><i class="${node.icon}"></i><p>${node.title}</p></a>`)
      
    }
    
    nodes = nodes.join('')
    
    navbar = `<nav>${nodes}</nav>`
    
    return navbar
    
  },

  generateCssLinks: async function(otherLinks=[]) {
    return new Promise((resolve, reject) => {
      fs.readdir('/proxy/app/ui/srv/styles/', (err, data) => {
        if(!err) {
          let links = ''
          for(let ss in data) {
            links += `<link href="/styles/${data[ss]}" rel="stylesheet" />`
          }
          if(otherLinks.length) {
            for(let ss in otherLinks) {
              links += `<link href="${otherLinks[ss]}" rel="stylesheet"/>`
            }
          }
          resolve(links)
        } else {
          reject('')
        }
      })
    })
  },
  generateScriptLinks: async function(otherScripts=[]) {
    return new Promise((resolve, reject) => {
      fs.readdir('/proxy/app/ui/srv/scripts/', (err, data) => {
        if(!err) {
          let scripts = ''
          for(let scr in data) {
            scripts += `<script src="/scripts/${data[scr]}"></script>`
          }
          if(otherScripts.length) {
            for(let scr in otherScripts) {
              scripts += `<script src="${otherScripts[scr]}"></script>`
            }
          }
          resolve(scripts)
        } else {
          reject('')
        }
      })
    })
  },
  
  generateHTMLHead: async function() {
    
    let otherSS = [
    'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
    ]
    
    let sSheets = await assembler.generateCssLinks(otherSS)
    let scripts = await assembler.generateScriptLinks()
    return `<head><title>RedProx</title>${sSheets}${scripts}</head>`
  },
  
  generateHTMLBody: function() {
    
    let nav = assembler.generateNavbar()
    let main = '<div id="main"></div>'
    
    return `<body>${nav}${main}</body>`
  },
  
  generatePage: async function() {
      
    let head = await assembler.generateHTMLHead()
    let body = assembler.generateHTMLBody()
    
    return `<!DOCTYPE html><html>${head}${body}</html>`
  }
}