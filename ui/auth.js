const crypto = require('crypto')
const pem = require('pem')

const tokenDuration = 3600000

const auth = module.exports = {
  encryptPassword: function(user, pass) {
    
    
    let hash = crypto.createHash('sha512')
    
    
    let iterations = Math.ceil((user.length + pass.length) * Math.PI)
    
    
    hash.update((Math.PI).toString())
    
    let salt = hash.digest('hex')
    
    let string = pass + user + salt
    
    for(let i=0; i < iterations; i++) {
      
      hash = crypto.createHash('sha512')
      
      hash.update(string + salt)
      
      string = hash.digest('hex')
      
    }
    return string
  },
  validate: {
    user: function(user, pass) {
      // generate token from user and pass
      // store token in redis in form of token:issuedTimestamp
      // return tokenString
      
      // if incorrect user/pass
      // return false
    },
    token: function(user, pass, tokenProvided) {
      
      let userClaims = authTokens[user]
      
      let tokenLocal = auth.token.encrypt(userClaims)
      
      if(token) {
        if(token.user == user) {
          
          let time = new Date().getTime()
          
          if(time < token + tokenDuration + 1800000) {
            return auth.generate.token(user)
          }
          
          return true
        }
      }
      
      return false
    }
  },
  generate: {
    
  },
    
    
    
    
    
  user: {
    changeUsername: function(name) {
      if(name.length > 0) {
        options.user.name = name
        config.save()
        return true
      }
      return false
    },
    changePassword: function(pass) {
      if(pass.length >= 0) {
        options.user.pass = encryptPass(options.user.name, pass)
        return true
      }
      return false
    },
    verifyPassword(user, pass) {
      if(options.user.name == user && pass.lengh >= 8) {
        if(encryptPass(user, pass) == options.user.pass) {
          return true
        }
      }
      return false
    }
  },
  
  generatePem: function() {
    return new Promise((resolve, reject) => {
      pem.createCertificate({ days: 14, selfSigned: true }, (err, keys) => {
        if (err) {
          reject(0)
        } else {
          resolve(keys)
        }
      })
    })
  }
    

}