# RedProx - A node.js based http(s) reverse proxy server

## Prerequisites

Node.js / npm

Redis-server

## Setup

Pull into /proxy/app/ directory

Rename proxy.conf.sample to proxy.conf and alter the JSON to your specification

## proxy.conf

### [definitions]
```
[definitions]: array containing the definitions for each domain name. This is where RedProx goes when a request is received to determine where to forward the request to
    [name]: string to identify definition (not yet implemented)
    [domain]: domain name to match incoming requests to
    [listener]: the name of the listener this definition is assigned to (see listeners)
    [server]: is the server to forward requests to for this
        [port]: port server is listening on
        [protocol]: protocol web server is listening for (currently only http or https)
        [ip]: ip address of server to forward to
    [enabled] flag for enabling/disabling definition (true/false)
```

### [listeners]
```
[listeners]: array containing each port/interface to create a listener on
    [name]: name to identify listener 
    [port]: port number to create listener on
    [protocol]: protocol to serve data with (currently only http or https)
    [ip]: ip address to create listener on
```
### [ssl]
```
[ssl]: paths to ssl private key/certificates
    [key]: path to private key file
    [cert]: path to ssl certificate
```
### [blacklist]
```
[blacklist]: configuration to control behaviour of ip blacklisting
    [file]: path to blacklist file (formatted as JSON array)
    [whitelist]: treat blacklist as a whitelist instead
    [trusted]: array containing ip addresses to always allow regardless of black/whitelisting
```
### [gui]
```
[gui]: contains config for gui (in progress and currently insecure)
    [enabled]: flag for enabling / disabling gui (true/false)
    [interface]: ip address to serve gui to
    [port]: port to listen on
    [pages]: array of pages to include in nav bar
        [title]: title of page
        [icon]: navbar icon
        [href]: href to page
```

### [user]
```
[user]: username / password hash for authentication to access gui (not yet implemented)
    [name]: username
    [pass]: password hash
```

### [commands]
```
[commands]: REST api enabled commands for gui (not active if gui is disabled)
    [enabled]: array of enabled commands
```




