'use strict'
const process = require('process');

const init = require('./init.js')


init.start()

async function gracefulExit(e) {
  await init.stop()
}

for (const event of ["exit", "SIGINT", "SIGUSR1", "SIGUSR2", "uncaughtException", "SIGTERM"]) {
  process.on(event, init.stop)
}