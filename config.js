'use strict'

const fs = require('fs')
const crypto = require('crypto')

const blacklist = require('./blacklist.js')

function addDefinition(defType, o) {
  let defI = options[defType].findIndex((def) => {
    return def.name === o.name
  })
  if(defI >= 0) {
    let oCurrent = options[defType][defI]
    options[defType][defI] = Object.assign({}, oCurrent, o)
  } else {
    options[defType].push(o)
  }
}
function removeDefinition(defType, n) {
  let defI = options[defType].findIndex((def) => {
    return def.name === n
  })
  if(defI != -1) {
    options[defType].splice(defI, 1)
    return true
  }
  return false
}
function encryptPass(user, pass) {
  let hash = crypto.createHash('sha512')
  let iterations = Math.ceil((user.length + pass.length) * Math.PI)
  hash.update((Math.PI).toString())
  let salt = hash.digest('hex')
  let string = pass + user + salt
  for(let i=0; i < iterations; i++) {
    hash = crypto.createHash('sha512')
    hash.update(string + salt)
    string = hash.digest('hex')
  }
  return string
}

const config = module.exports = {
  save: async function() {
    try {
      let configFile = fs.readFileSync('/proxy/app/proxy.conf').toString
      let o = JSON.stringify(options, null, 2)
      
      if(configFile != o) {
        console.log("Config changed")
      } else {
        fs.writeFileSync('/proxy/app/proxy.conf', o)
        console.log('Saved "proxy.conf"')
      }
    } catch(e) {
      console.warn('Error saving "proxy.conf"')
    }
  },
  load: function() {
    let file = '/proxy/app/proxy.conf'
    if(fs.existsSync(file)) {
      try {
        let options = JSON.parse(fs.readFileSync(file).toString())
        global.options = options
        
        if(options.ssl.key != null && options.ssl.cert != null) {
          let key = fs.readFileSync(options.ssl.key)
          let cert = fs.readFileSync(options.ssl.cert)
          global.ssl = {key, cert}
        }
        console.log('Loaded "proxy.conf"')
      } catch(e) {
        console.error('Failed to parse "proxy.conf"')
      }
    } else {
      throw 'ERROR: "proxy.conf" does not exist'
    }
  },
  definition: {
    server: {
      add: function(o) {
        /*
          name,
          domain,
          proxy
            port,
            protocol,
            ip,
          server
            port,
            serverProtocol,
            serverIp,
          enabled
        */
        addDefinition('servers', o)
        config.save()
        return true
      },
      remove: function(name) {
        if(removeDefinition('servers', name)) {
          config.save()
          return true
        }
        return false
      }
    },
    port: {
      add: function(o) {
        /*
          name,
          port,
          protocol,
          ip
        */
        addDefinition('ports', o)
        config.save()
        return true
      },
      remove: function(name) {
        if(removeDefinition('ports', name)) {
          config.save()
          return true
        }
        return false
      }
    },
    interface: {
      add: function(o) {
        /*
          name,
          ip
        */
        addDefinition('interfaces', o)
        config.save()
        return true
      },
      remove: function(name) {
        if(removeDefinition('interfaces', name)) {
          config.save()
          return true
        }
        return false
      }
    }
  }, 
  mimeTypes: {
    '.aac'	:	'audio/aac',
    '.abw'	:	'application/x-abiword',
    '.arc'	:	'application/x-freearc',
    '.avi'	:	'video/x-msvideo',
    '.azw'	:	'application/vnd.amazon.ebook',
    '.bin'	:	'application/octet-stream',
    '.bmp'	:	'image/bmp',
    '.bz'	:	'application/x-bzip',
    '.bz2'	:	'application/x-bzip2',
    '.csh'	:	'application/x-csh',
    '.css'	:	'text/css',
    '.csv'	:	'text/csv',
    '.doc'	:	'application/msword',
    '.docx'	:	'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    '.eot'	:	'application/vnd.ms-fontobject',
    '.epub'	:	'application/epub+zip',
    '.gif'	:	'image/gif',
    '.htm'	:	'text/html',
    '.html'	:	'text/html',
    '.ico'	:	'image/vnd.microsoft.icon',
    '.ics'	:	'text/calendar',
    '.jar'	:	'application/java-archive',
    '.jpeg'	:	'image/jpeg',
    '.jpg'	:	'image/jpeg',
    '.js'	:	'text/javascript',
    '.json'	:	'application/json',
    '.jsonld'	:	'application/ld+json',
    '.mid'  :	'audio/midi audio/x-midi',
    '.midi'	:	'audio/midi audio/x-midi',
    '.mjs'	:	'text/javascript',
    '.mp3'	:	'audio/mpeg',
    '.mpeg'	:	'video/mpeg',
    '.mpkg'	:	'application/vnd.apple.installer+xml',
    '.odp'	:	'application/vnd.oasis.opendocument.presentation',
    '.ods'	:	'application/vnd.oasis.opendocument.spreadsheet',
    '.odt'	:	'application/vnd.oasis.opendocument.text',
    '.oga'	:	'audio/ogg',
    '.ogv'	:	'video/ogg',
    '.ogx'	:	'application/ogg',
    '.otf'	:	'font/otf',
    '.png'	:	'image/png',
    '.pdf'	:	'application/pdf',
    '.ppt'	:	'application/vnd.ms-powerpoint',
    '.pptx'	:	'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    '.rar'	:	'application/x-rar-compressed',
    '.rtf'	:	'application/rtf',
    '.sh'	  :	'application/x-sh',
    '.svg'	:	'image/svg+xml',
    '.swf'	:	'application/x-shockwave-flash',
    '.tar'	:	'application/x-tar',
    '.tif'  :	'image/tiff',
    '.tiff'	:	'image/tiff',
    '.ttf'	:	'font/ttf',
    '.txt'	:	'text/plain',
    '.vsd'	:	'application/vnd.visio',
    '.wav'	:	'audio/wav',
    '.weba'	:	'audio/webm',
    '.webm'	:	'video/webm',
    '.webp'	:	'image/webp',
    '.woff'	:	'font/woff',
    '.woff2'	:	'font/woff2',
    '.xhtml'	:	'application/xhtml+xml',
    '.xls'	:	'application/vnd.ms-excel',
    '.xlsx'	:	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    '.xml'	:	'text/xml',
    '.xul'	:	'application/vnd.mozilla.xul+xml',
    '.zip'	:	'application/zip',
    '.7z'	:	'application/x-7z-compressed'
  }
}