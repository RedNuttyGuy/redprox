const redis = require('redis')
const fs = require('fs')

global.redisClient = redis.createClient({password: "UW85VGJ!ef&74nJGoj@6%rm^6GowjhGk"})
const key = 'blacklist'

const rejectTimer = 10

redisClient.on('connect', function() {
  console.log('Redis client connected');
})
redisClient.on('error', function (err) {
    console.log(err);
})

blacklist = module.exports = {
  save: async function() {
    try {
      let list = await this.blacklist()
      listString = JSON.stringify(list, null, 2)
      fs.writeFileSync(options.blacklist.file, listString)
      console.log('Saved "blacklist.conf"')
    } catch(e) {
      console.warn('Error saving "blacklist.conf"')
    }
  },
  load: function() {
    let file = options.blacklist.file
    if(fs.existsSync(file)) {
      try {
        let list = JSON.parse(fs.readFileSync(file).toString())
        this.addMulti(list)
        console.log('Loaded "blacklist.conf"')
      } catch(e) {
        console.warn('Error parsing "blacklist.conf"')
      }
    } else {
      console.log('Creating "blacklist.conf"')
      fs.writeFileSync(file, "[]")
    }
  },
  add: function(ip) {
    redisClient.sadd(key, ip)
  },
  addMulti: function(arr) {
    for(ip in arr) {
      this.add(arr[ip])
    }
  },
  remove: function(ip) {
    redisClient.srem(key, ip)
  },
  modify: function(oldIp, newIp) {
    redisClient.sadd(key, newIp)
    redisClient.srem(key, oldIp)
  },
  localIp: function(ip) {
    
    let ipSplit = ip.split('.')
    
    if(ipSplit[0] == 10) {
      return true
    }
    if(ipSplit[0] == 172 && ipSplit[1] <= 31 && ipSplit[1] >= 16) {
      return true
    }
    if(ipSplit[0] == 192 && ipSplit[1] == 168) {
      return true
    }
    return false
  },
  blacklisted: function(ip) {
    if(ip === undefined) {
      return !options.blacklist.whitelist
    }
    return new Promise((resolve, reject) => {
      redisClient.sismember(key, ip, (err, reply) => {
        if(err) {
          reject(!options.blacklist.whitelist)
        } else {
          resolve(!!reply)
        }
      })
    })
  },

  blacklist: function() {
    return new Promise(function(resolve, reject) {
      redisClient.smembers(key, (err, reply) => {
        if(err) {
          reject(err)
        } else {
          resolve(reply)
        }
      })
    })
  },

  reject: {

    add: function(ip) {
      let k = 'blacklist.reject:' + ip
      redisClient.incr(k, (err, reply) => {
        redisClient.expire(k, rejectTimer)
      })
    },

    count: function(ip) {
      return new Promise(function(resolve, reject) {
        redisClient.get('blacklist.reject:' + ip, (err, reply) => {
          if(err) {
            reject(err)
          } else {
            resolve(reply)
          }
        })
      })
    }
  }
}
