const util = module.exports = {
  readdirRecursive: function(dir) {

    let fileList = []
    
    async function read(d) {
      
      let list = fs.readdirSync(d)
      
      for(let i in list) {
        let item = list[i]
        if(lstatSync(item).isDirectory()) {
          read(item)
        } else {
          fileList.push(item)
        }
      }
    }(dir)
    
    return fileList
  }
}