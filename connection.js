'use strict'

const http = require('http')
const https = require('https')
const blacklist = require('./blacklist.js')

const connection = module.exports = {
  approve: {
    wan: async function(origin, destination, path, definitions=options.definitions) {
      let trusted = options.blacklist.trusted.includes(origin)
      
      if(options.blacklist.whitelist && !trusted) {
        trusted = await blacklist.blacklisted(origin)
        console.log(trusted)
        
        if(!trusted) {
          return {kill: true,  reason: 'notWhitelisted'}
        }
      }
      
      if(!trusted) {
        if( await blacklist.blacklisted(origin) ) { return {kill: true,  reason: 'blacklisted'} }
        if( origin == null || destination == null ) { return {kill: true, reason: 'invalidHeader'} }
        if( path.includes('../') ) { return {kill: true, reason: 'fsTraversal'} }
      }
      
      let pass = definitions.find((definition) => { 
        return definition.domain === destination
      })
      
      if( trusted ) { return {kill: false, reason: 'trusted', forward: pass} }
      if( !pass ) { return {kill: true, reason: 'noDefinition'} }
      if( !pass.enabled ) { return {kill: true, reason: 'disabled'} }
      return {kill: !Boolean(pass), reason: 'pass', forward: pass}
    },
    lan: async function(origin, definitions=options.definitions) {
      let trusted = options.blacklist.trusted.includes(origin)
      
      if(!trusted && options.blacklist.whitelist) {
        trusted = blacklist.blacklisted(origin)
      }
      if(!trusted) {
        if( await blacklist.blacklisted(origin)) { return {kill: true, reason: 'blacklisted'} }
        if( origin == null || destination == null) { return {kill: true, reason: 'invalidHeader'} }
        if( path.includes('..')) { return {kill: true, reason: 'fsTraversal'} }
        if( connection.check.local(origin) ) { return {kill: false, reason: 'local'} }
      }
      
      let pass = definitions.find((definition) => { 
        return definition.domain === destination
      })
      
      if( trusted ) { return {kill: false, reason: 'trusted', forward: pass} }
      if( !pass ) { return {kill: true, reason: 'noDefinition'} }
      if( !pass.enabled ) { return {kill: true, reason: 'disabled'}}
      return {kill: !Boolean(pass), reason: 'pass', forward: pass}
    }
  },
  check: {
    local: function(ip) {
      let ipSplit = ip.split('.')
      
      if(ipSplit[0] == 10) {
        return true
      }
      if(ipSplit[0] == 172 && ipSplit[1] <= 31 && ipSplit[1] >= 16) {
        return true
      }
      if(ipSplit[0] == 192 && ipSplit[1] == 168) {
        return true
      }
      return false
    }
  },
  response: {
    notFound: function(req, res) {
      if(!req.finished && !res.finished) {
        res.writeHead(404, {'Content-Type':'text/html'})
        res.end(`<h1>404</h1><h3>${req.headers.host} does not exist</h3>`)
      }
      return
    },
    reflect: function(req, res) {
      if(!req.finished && !res.finished) {
        res.writeHead(302, {location: 'http://' + req.connection.remoteAddress + req.url})
        res.end('')
      }
      return
    },
    redirect: function(req, res, destination) {
      if(!req.finished && !res.finished) {
        res.writeHead(302, {location: destination})
        res.end('')
      }
      return
    },
    internalError: function(req, res) {
      if(!req.finished && !res.finished) {
        res.writeHead(500, {'Content-Type':'text/html'})
        res.end('<h1>500</h1><h3>Something Went Wrong.<br/>Sorry...</h3>')
      }
      return
    },
    pass: function(req, res, serverRes) {
      if(!req.finished && !res.finished && !serverRes.finished) {
        res.writeHead(serverRes.statusCode, serverRes.headers)
        serverRes.pipe(res, {
          end: true
        });
      }
      return
    }
    
  },
  reject: async function(req, res, reason) {
    
    if(reason == 'local') {
      connection.response.notFound(req, res)
      return
    }
    if(reason == 'blacklisted') {
      connection.response.reflect(req, res)
      return
    }
    if(reason == 'invalidHeader') {
      connection.response.reflect(req, res)
      blacklist.add(req.connection.remoteAddress)
      return
    }
    if(reason == 'disabled') {
      connection.response.internalError(req, res)
      return
    }
    if(await blacklist.reject.count(req.connection.remoteAddress) >= 4) {
      connection.response.reflect(req, res)
      blacklist.add(req.connection.remoteAddress)
      return
    }
    
    connection.response.notFound(req, res)
    blacklist.reject.add(req.connection.remoteAddress)
    return
  },
  request: {
    http: function(req, res, options) {
      let serverRequest = http.request(options, (serverResponse) => {
        if(serverResponse.statusCode == 404) {
          blacklist.reject.add(req.connection.remoteAddress)
        }
        connection.response.pass(req, res, serverResponse)
      })
      serverRequest.on('error', (err) => {
        connection.response.internalError(req, res)
        return
      })
      return serverRequest
    },
    https: function(req, res, options) {
      let serverRequest = https.request(options, (serverResponse) => {
        if(serverResponse.statusCode == 404) {
          blacklist.reject.add(req.connection.remoteAddress)
        }
        connection.response.pass(req, res, serverResponse)
      })
      serverRequest.on('error', (err) => {
        connection.response.internalError(req, res)
        return
      })
      return serverRequest
    }
  }
}
