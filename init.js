'use strict'
const fs = require('fs')
const process = require('process')
const config = require('./config.js')
const blacklist = require('./blacklist.js')
const listener = require('./listener.js')
const ui = {server: require('./ui/server.js')}

const init = module.exports = {
  start: function(){
    config.load()
    blacklist.load()
    global.mimeTypes = config.mimeTypes
    
    listener.init()
    
    if(options.gui.enabled) {
      ui.server.startGui()
    }
  },
  
  stop: async function() {
    await config.save()
    await blacklist.save()
    listener.stopAll()
    ui.server.stopGui()
    process.exit()
  }
}