'use strict'

const http = require('http')
const https = require('https')
const url = require('url')

const connection = require('./connection.js')

const listener = module.exports = {
  init: function() {
    
    global.listeners = {}
    
    for(let l in options.listeners) {
      let {name, port, protocol, ip} = options.listeners[l]
      let definitions = options.definitions.filter((d) => {
        return (d.enabled && d.listener === name)
      })
      
      let listener = this.create[protocol](definitions)
      
      listeners[name] = {listener, port, protocol, ip, definitions}
    }
    
    this.startAll()
  },
  create: {
    http: function(listener) {
      let httpServer = http.createServer(async (req, res) => {
        
        let logInfo = {
          time: new Date().getTime(),
          origin: req.connection.remoteAddress,
          destination: req.headers.host,
          resource: req.url
        }
        
        let reqHostname = url.parse(`http://${req.headers.host}${req.url}`)
        
        let approval = await connection.approve.wan(
          req.connection.remoteAddress,
          req.headers.host,
          req.url,
          listener.definitions
        )
        if(approval.kill) {
          connection.reject(req, res, approval.reason)
          console.log(`REJECTION: ${approval.reason}`)
          console.log(logInfo)
          return
        }
        let { forward } = approval
        
        if(listeners[forward.listener].protocol == 'https' && !req.url.includes('.well-known')) {
          connection.response.redirect(req, res, `https://${req.headers.host}${req.url}`)
          return
        }
        
        
        let options = {
          hostname: forward.server.ip,
          port: forward.server.port,
          path: req.url,
          method: req.method,
          headers: req.headers,
          rejectUnauthorized: false
        };
        
        
        options.headers['X-Forwarded-Host'] = `${listener.protocol}://${options.hostname}:${options.port}`
        options.headers['X-Forwarded-Proto'] = 'http'
        options.headers['X-Real-IP'] = req.connection.remoteAddress
        options.headers['referrer'] = ''
        options.headers['origin'] = ''
        
        let serverRequest
        
        if(forward.server.protocol === 'http') {
          serverRequest = connection.request.http(req, res, options)
        }
        
        if(forward.server.protocol === 'https') {
          serverRequest = connection.request.https(req, res, options)
        }
        
        req.pipe(serverRequest, {
          end: true
        });
        return
      })
      return httpServer
    },
    https: function(listener) {
      let httpsServer = https.createServer({key: ssl.key, cert: ssl.cert}, async (req, res) => {
        
        let logInfo = {
          time: new Date().getTime(),
          origin: req.connection.remoteAddress,
          destination: req.headers.host,
          resource: req.url
        }
        
        let reqHostname = url.parse(`https://${req.headers.host}${req.url}`)
        
        let approval = await connection.approve.wan(
          req.connection.remoteAddress,
          reqHostname.hostname,
          req.url,
          listener.definitions
        )
        
        if(approval.kill) {
          await connection.reject(req, res, approval.reason)
          console.log(`REJECTION: ${approval.reason}`)
          console.log(logInfo)
          return
        }
        
        let { forward } = approval
        
        let options = {
          hostname: forward.server.ip,
          port: forward.server.port,
          path: req.url,
          method: req.method,
          headers: req.headers,
          rejectUnauthorized: false
        };
        
        options.headers['X-Forwarded-Host'] = `${listener.protocol}://${options.hostname}:${options.port}`
        options.headers['X-Forwarded-Proto'] = `https`
        options.headers['X-Real-IP'] = req.connection.remoteAddress
        options.headers['referrer'] = ''
        options.headers['origin'] = ''
        
        let serverRequest
        
        if(forward.server.protocol === 'http') {
          serverRequest = connection.request.http(req, res, options)
        }
        
        if(forward.server.protocol === 'https') {
          serverRequest = connection.request.https(req, res, options)
        }
        
        req.pipe(serverRequest, {
          end: true
        });
        return
      })
      return httpsServer
    }
  },
  start: function (name) {
    if(listeners[name]) {
      let { listener, port, ip } = listeners[name]
      listener.listen(port, ip)
    }
  },
  stop: function(name) {
    if(listeners[name]) {
      listeners[name].listener.close()
    }
  },
  startAll: function () {
    for(let l in listeners) {
      this.start(l)
    }
  },
  stopAll: function() {
    for(let l in listeners) {
      this.stop(l)
    }
  }
}